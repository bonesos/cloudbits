Most of the content here borrows / re-uses the work done by the cirros
project:  http://cirros-cloud.net/

Some of the files may be slightly modified to suite the needs of this project.

Many thanks to the developers of cirros for such a great product.

This project contains scripts and utilities to bootstrap an instance in a cloud
environment.  Since BonesOS focuses primarily on operating in LXC/LXD,
these feature are intended for use within an OpenStack cloud running nova-lxd
or other suitable hypervisor to launch an lxc image.